from contextlib import contextmanager
import shlex
import os
from pathlib import Path
from typing import List, Tuple, Generator

import pytest
import subprocess
import datetime

from pytest_cookies.plugin import Cookies, Result  # type: ignore

_DEPENDENCY_FILE = "pyproject.toml"
_INSTALL_DEPS_COMMANDS = [
    "poetry install --no-interaction",
]
LINE_LENGTH = 120
PYPI_URL = "https://pypi.python.org/api/pypi/pypi/simple"
PROJECT_NAME = "Demo Python Package"
PROJECT_SLUG = "demo_python_package"
PROJECT_USERNAME = "mark doerr"
PROJECT_PATH = "https://gitlab.com/opensourcelab/software-dev/{{ cookiecutter.project_name.lower().replace(' ', '-') }}"
PYTHON_VERSION = 3.9
#MYPY_VERSION = "^0.971"
#PYLINT_VERSION = "^2.14.5"
#PYTEST_VERSION = "^7.1.2"


def build_commands(commands: List[str]) -> List[str]:
    """
    `Build command list from list of strings.`

    The function takes a list of strings and returns a list of strings

    :param List[str] commands: List of strings to build the command list from
    :return: A list of strings.
    """
    cmds = _INSTALL_DEPS_COMMANDS.copy()
    cmds.extend(commands)
    return cmds


@contextmanager
def change_working_directory(path: str) -> Generator:
    """
    It changes the working directory to the path specified, and then returns to the previous working directory when the
    function exits

    :param str path: The path of the directory the command is being run
    """
    prev_cwd = Path.cwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(prev_cwd)


def run_inside_dir(commands: List[str], path: str) -> None:
    """
    It runs a command from inside a given directory, returning the exit status.

    :param List[str] commands: A list of commands to run
    :param str path: The path of the directory the command is being run
    """
    with change_working_directory(path):
        for command in commands:
            try:
                subprocess.check_call(shlex.split(command))
            except subprocess.CalledProcessError:
                raise


def check_output_inside_dir(command: str, path: str) -> bytes:
    """
    It runs a command from inside a given directory, returning the command output

    :param str command: The command to run
    :param str path: The path to the directory you want to run the command from
    :return: The output of the command.
    """
    with change_working_directory(path):
        try:
            return subprocess.check_output(shlex.split(command))
        except subprocess.CalledProcessError:
            raise


def found_toplevel_files(result: Result) -> List[str]:
    """
    It returns a list of all files in the project

    :param List[str] result: A list of top level files in the project
    """
    return [f.name for f in result.project_path.glob("**/*") if f.is_file()]


def found_toplevel_dirs(result: Result) -> List[str]:
    """
    It returns a list of toplevel directories in the project

    :param List[str] result: A list of top level directories in the project
    """
    return [f.name for f in result.project_path.iterdir() if f.is_dir()]


def bake_result(cookies, *args, **kwargs) -> Result:
    """
    It takes a cookiecutter project and a set of arguments, bakes the project, and asserts that the bake was successful

    :param Cookies cookies: Cookies instance
    :return: The result of the bake function.
    """
    result = cookies.bake(*args, **kwargs)
    assert result.exception is None, result.exception
    assert result.exit_code == 0, result.exit_code
    assert result.project_path.is_dir()
    return result


def test_bake_with_defaults(cookies: Cookies) -> None:
    """
    It asserts that the generated project contains the files and directories that we expect

    :param Cookies cookies: Cookies instance
    """
    result = bake_result(cookies)

    output_files = found_toplevel_files(result)
    assert _DEPENDENCY_FILE in output_files
    assert ".flake8" in output_files
    assert ".gitlab-ci.yml" in output_files
    assert "Dockerfile" in output_files
    assert "Makefile" in output_files
    # These aren't in the default project:
    # assert "AUTHORS.rst" in output_files
    assert "AUTHORS.md" in output_files
    #assert "LICENSE" not in output_files
    assert "LICENSE" in output_files
    assert ".pylintrc" not in output_files
    assert "setup.cfg" in output_files
    assert "setup.py" in output_files
    assert "VERSION" not in output_files

    output_dirs = found_toplevel_dirs(result)
    assert "docs" in output_dirs
    assert "tests" in output_dirs
    assert f"{PROJECT_SLUG}" in output_dirs


def test_bake_with_all_options_and_setuptools(cookies: Cookies) -> None:
    """
    It asserts that the generated project contains the files and directories that we expect

    :param Cookies cookies: Cookies instance
    """
    extra_context = {
        "use_mypy": "y",
        "use_pylint": "y",
        "create_author_file": "y",
        "build_backend": "setuptools",
        "open_source_license": "MIT",
    }
    result = bake_result(cookies, extra_context=extra_context)

    output_files = found_toplevel_files(result)
    assert "AUTHORS.rst" in output_files
    assert "LICENSE" in output_files
    assert ".pylintrc" in output_files
    assert "setup.cfg" in output_files
    assert "setup.py" in output_files
    assert "VERSION" in output_files


def test_bake_with_defaults_pyproject(cookies: Cookies) -> None:
    """
    It tests that the generated `pyproject.toml` file contains the expected dependencies and configuration

    :param Cookies cookies: Cookies instance
    """
    result = bake_result(cookies)
    dep_file_path = result.project_path / _DEPENDENCY_FILE
    dep_file = dep_file_path.read_text()
    assert f"line-length = {LINE_LENGTH}\n" in dep_file
    assert f'url = "{PYPI_URL}"\n' in dep_file
    # These aren't in the default project:
    #assert f'mypy = "{MYPY_VERSION}"\n' not in dep_file
    #assert f'pylint = "{PYLINT_VERSION}"\n' not in dep_file


def test_bake_year_compute_in_license_file(cookies: Cookies) -> None:
    """
    It checks that the current year is in the license file

    :param Cookies cookies: Cookies instance
    """
    result = bake_result(cookies, extra_context={"open_source_license": "MIT"})
    license_file_path = result.project_path / "LICENSE"
    now = datetime.datetime.now()
    assert str(now.year) in license_file_path.read_text()


@pytest.mark.parametrize("extra_context", [{}, {"full_name": 'name "quote" name'}, {"full_name": "O'connor"}])
def test_bake_tests(cookies: Cookies, extra_context: List[dict]) -> None:
    """
    It tests that the `pytest` dependency is installed and that the test file contains the `import pytest` statement

    :param Cookies cookies: Cookies instance
    :param List[dict] extra_context: A dictionary of values to be passed to the template
    """
    result = bake_result(cookies, extra_context=extra_context)
    # Test pyproject installs pytest
    dep_file_path = result.project_path / _DEPENDENCY_FILE
    dep_file = dep_file_path.read_text()
    #assert f'pytest = "{PYTEST_VERSION}"\n' in dep_file
    # Test contents of test file
    test_file_path = result.project_path / f"tests/test_{PROJECT_SLUG}.py"
    test_file = test_file_path.read_text()
    assert "def test_version():\n" in test_file


def test_bake_without_author_file(cookies: Cookies) -> None:
    """
    It tests that the `AUTHORS.rst` file is not created when the `create_author_file` option is set to `n`

    :param Cookies cookies: Cookies instance
    """
    result = bake_result(cookies, extra_context={"create_author_file": "n"})
    output_files = found_toplevel_files(result)
    assert "AUTHORS.rst" not in output_files
    doc_files = [f.name for f in (
        result.project_path / "docs").glob("**/*") if f.is_file()]
    assert "authors.rst" not in doc_files

    # Assert there are no spaces in the toc tree
    docs_index_path = result.project_path / "docs/index.rst"
    with open(str(docs_index_path)) as index_file:
        assert "development\n   history\n" in index_file.read()


def test_bake_with_author_file(cookies: Cookies) -> None:
    """
    It tests that the `AUTHORS.rst` file is created when the `create_author_file` option is set to `y`

    :param Cookies cookies: Cookies instance
    """
    result = bake_result(cookies, extra_context={"create_author_file": "y"})
    output_files = found_toplevel_files(result)
    assert "AUTHORS.rst" in output_files
    doc_files = [f.name for f in (
        result.project_path / "docs").glob("**/*") if f.is_file()]
    assert "authors.rst" in doc_files

    # Assert there are no spaces in the toc tree
    docs_index_path = result.project_path / "docs/index.rst"
    with open(str(docs_index_path)) as index_file:
        assert "authors\n   history\n" in index_file.read()


@pytest.mark.parametrize(
    "license_info",
    [
        ("MIT", "MIT "),
        (
            "BSD-3-Clause",
            "Redistributions of source code must retain the above copyright notice, this",
        ),
        ("ISC", "ISC License"),
        ("Apache-2.0", "Licensed under the Apache License, Version 2.0"),
        ("GPL-3.0-only", "GNU GENERAL PUBLIC LICENSE"),
    ],
)
def test_bake_selecting_license(cookies: Cookies, license_info: List[Tuple[str]]) -> None:
    """
    It takes a cookie and a license info tuple, and then it bakes a cookie with the license info, and then it checks
    that the license file contains the license string and that the dependency file contains the license string

    :param Cookies cookies: Cookies instance
    :param List[Tuple[str]] license_info: A list of license info and their expected strings
    """
    license_str, target_string = license_info
    result = bake_result(cookies, extra_context={
                         "open_source_license": license_str})
    license_file_path = result.project_path / "LICENSE"
    license_file = license_file_path.read_text()
    dep_file_path = result.project_path / _DEPENDENCY_FILE
    toml_file = dep_file_path.read_text()
    assert target_string in license_file
    assert license_str in toml_file


def test_bake_not_open_source(cookies: Cookies) -> None:
    """
    It tests that the generated project does not contain a license file or a license in the README or the dependency
    file if the user chooses "Not open source" as the license

    :param Cookies cookies: Cookies instance
    """
    result = bake_result(cookies, extra_context={
                         "open_source_license": "Not open source"})
    output_files = found_toplevel_files(result)
    assert _DEPENDENCY_FILE in output_files
    assert "LICENSE" not in output_files
    assert "License" not in (result.project_path / "README.rst").read_text()
    assert "license" not in (result.project_path /
                             _DEPENDENCY_FILE).read_text()


def test_bake_docs_config(cookies: Cookies) -> None:
    """
    It asserts that the docs config file is created and contains the correct data

    :param cookies: Cookies
    :type cookies: Cookies
    """
    result = bake_result(cookies)
    docs_index_path = result.project_path / "docs/conf.py"
    with open(str(docs_index_path)) as docs_conf:
        docs_string = docs_conf.read()
        assert f"import {PROJECT_SLUG}\n" in docs_string
        assert f'copyright = "2022, {PROJECT_USERNAME}"\n' in docs_string
        assert '    "logo": "genomics-england-logo.png",\n' in docs_string


def test_bake_invoke_docker_build(cookies: Cookies) -> None:
    """
    It checks that the `invoke docker-build` command works

    :param Cookies cookies: Cookies instance
    """
    result = bake_result(cookies)
    invoke_command = str(check_output_inside_dir(
        "invoke docker-build --dry", str(result.project_path)))
    assert (
        f"docker build --build-arg PYTHON_BASE={PYTHON_VERSION} --build-arg"
        " PYPI_URL=https://artifactory.aws.gel.ac/artifactory/api/pypi/pypi/simple --platform linux/amd64"
        " --cache-from"
        f" {PROJECT_PATH}/{PROJECT_NAME}/{PROJECT_NAME}:py{PYTHON_VERSION}-latest"
        " -f Dockerfile -t"
        f" {PROJECT_PATH}/{PROJECT_NAME}/{PROJECT_NAME}:py{PYTHON_VERSION}-latest"
        " --target test ."
    ) in invoke_command


def test_bake_invoke_docker_push(cookies: Cookies) -> None:
    """
    It tests that the `invoke docker-push` command works

    :param Cookies cookies: Cookies instance
    """
    result = bake_result(cookies)
    invoke_command = str(check_output_inside_dir(
        "invoke docker-push --dry", str(result.project_path)))
    assert "docker push" f" {PROJECT_PATH}/{PROJECT_NAME}/{PROJECT_NAME}:py{PYTHON_VERSION}-latest" in invoke_command


def test_bake_invoke_docker_pull(cookies: Cookies) -> None:
    """
    It tests that the `invoke docker-push` command works

    :param Cookies cookies: Cookies instance
    """
    result = bake_result(cookies)
    invoke_command = str(check_output_inside_dir(
        "invoke docker-pull --dry", str(result.project_path)))
    assert (
        "docker pull"
        f" {PROJECT_PATH}/{PROJECT_NAME}/{PROJECT_NAME}:py{PYTHON_VERSION}-latest"
        ' || echo "No pre-made image available"'
    ) in invoke_command


def test_bake_invoke_docker_test(cookies: Cookies) -> None:
    """
    It checks that the `invoke docker-test` command works

    :param Cookies cookies: Cookies instance
    """
    result = bake_result(cookies)
    invoke_command = str(check_output_inside_dir(
        "invoke docker-test --dry", str(result.project_path)))
    assert "docker run --platform linux/amd64 --volume" in invoke_command
    assert f"{PROJECT_PATH}/{PROJECT_NAME}/{PROJECT_NAME}:py{PYTHON_VERSION}-latest" in invoke_command
    assert f"pytest -v --cov-report xml:/{PROJECT_SLUG}/bin/coverage.xml" in invoke_command


def test_bake_invoke_docker_shell(cookies: Cookies) -> None:
    """
    It checks that the `invoke docker-shell` command works

    :param Cookies cookies: Cookies instance
    """
    result = bake_result(cookies)
    invoke_command = str(check_output_inside_dir(
        "invoke docker-shell --dry", str(result.project_path)))
    assert "docker run -it --platform linux/amd64 --volume" in invoke_command
    assert f"{PROJECT_PATH}/{PROJECT_NAME}/{PROJECT_NAME}:py{PYTHON_VERSION}-latest" in invoke_command
    assert "/bin/bash" in invoke_command


@pytest.mark.skipif(
    "not config.getoption('--run-slow')",
    reason="This takes a long time so best to run manually with `pytest -sv --run-slow -k test_bake_and_run_invoke`",
)
@pytest.mark.parametrize(
    "command",
    [
        "poetry run invoke format --check",
        "poetry run invoke lint",
        "poetry run invoke security",
        'poetry run invoke test --junit --coverage="xml"',
        "poetry run invoke docs --no-launch",
    ],
)
@pytest.mark.parametrize(
    "build_backend",
    [
        "setuptools",
        "poetry",
    ],
)
def test_bake_and_run_invoke(cookies: Cookies, command: str, build_backend: str) -> None:
    """
    It creates a new project with all options set, and then runs the commands to format, lint, test, and build docs

    :param Cookies cookies: Cookies instance
    :param str command: The invoke command to run
    :param str build_backend: The build backend to test (poetry | setuptools)
    """
    extra_context = {
        "use_mypy": "y",
        "use_pylint": "y",
        "create_author_file": "y",
        "build_backend": build_backend,
        "open_source_license": "MIT",
    }
    result = bake_result(cookies, extra_context=extra_context)
    commands = build_commands([command])
    run_inside_dir(commands, str(result.project_path))


@pytest.mark.skipif(
    "not config.getoption('--run-slow')",
    reason="This takes a long time so best to run manually with `pytest -sv --run-slow -k test_bake_makefile_build`",
)
@pytest.mark.parametrize(
    "build_backend",
    [
        "setuptools",
        "poetry",
    ],
)
def test_bake_makefile_build(cookies: Cookies, build_backend: str) -> None:
    """
    It runs the `make` command inside the project directory and checks that the output contains the help message

    :param Cookies cookies: Cookies instance
    """
    extra_context = {
        "build_backend": build_backend,
    }
    result = bake_result(cookies, extra_context=extra_context)
    make_output = str(check_output_inside_dir(
        "make", str(result.project_path)))
    assert "Prints this help/overview message\\n" in make_output
    commands = build_commands(
        [
            "CI_COMMIT_REF_NAME=latest make build",
            "CI_COMMIT_REF_NAME=latest make test",
        ]
    )
    with change_working_directory(str(result.project_path)):
        for command in commands:
            try:
                subprocess.check_call(command, shell=True)
            except subprocess.CalledProcessError:
                raise


@pytest.mark.parametrize(
    "build_backend",
    [
        (
            "setuptools",
            '\n[build-system]\nrequires = ["setuptools>=42"]\nbuild-backend = "setuptools.build_meta"\n',
        ),
        (
            "poetry",
            '\n[build-system]\nrequires = ["poetry>=0.12"]\nbuild-backend = "poetry.masonry.api"\n',
        ),
    ],
)
def test_bake_build_backend_pyproject_build_system(cookies: Cookies, build_backend: List[Tuple[str]]) -> None:
    """
    It tests that the build system is correctly set in the `pyproject.toml` file

    :param Cookies cookies: The fixture that returns a Cookiecutter instance
    :param build_backend: A tuple of the build backend option and the string that should be in the pyproject.toml file
    """
    build_option, build_system_str = build_backend
    result = bake_result(cookies, extra_context={
                         "build_backend": build_option})
    dep_file_path = result.project_path / _DEPENDENCY_FILE
    toml_file = dep_file_path.read_text()
    assert build_system_str in toml_file


def test_bake_build_poetry_backend_dockerfile(cookies: Cookies) -> None:
    """
    It tests that the COPY files is set correct in Dockerfile

    :param Cookies cookies: The fixture that returns a Cookiecutter instance
    """
    result = bake_result(cookies, extra_context={"build_backend": "poetry"})
    dockerfile = result.project_path / "Dockerfile"
    dockerfile_text = dockerfile.read_text()
    file_copy_str = f"\nCOPY pyproject.toml poetry.lock README.rst /{PROJECT_SLUG}/\n"
    install_package_str = "\nRUN poetry install --no-dev\n\nFROM base as test\n"
    install_dev_package_str = (
        "\nRUN pip install -Iv --prefer-binary --index-url $PYPI_URL --upgrade \\\n"
        "    pip \\\n    tomlkit \\\n    virtualenv \\\n    requests \\\n    && poetry install\n"
    )

    assert file_copy_str in dockerfile_text
    assert install_package_str in dockerfile_text
    assert install_dev_package_str in dockerfile_text


def test_bake_build_setuptools_backend_dockerfile(cookies: Cookies) -> None:
    """
    It tests that the COPY files is set correct in Dockerfile

    :param Cookies cookies: The fixture that returns a Cookiecutter instance
    """
    result = bake_result(cookies, extra_context={
                         "build_backend": "setuptools"})
    dockerfile = result.project_path / "Dockerfile"
    dockerfile_text = dockerfile.read_text()
    file_copy_str = f"\nCOPY pyproject.toml poetry.lock README.rst setup.cfg setup.py VERSION /{PROJECT_SLUG}/\n"
    install_package_str = """
RUN poetry export --without dev --without-hashes -f requirements.txt -o requirements.txt \\
    && poetry export --only dev --without-hashes -f requirements.txt -o requirements_dev.txt \\
    && python -m pip install --prefer-binary --index-url $PYPI_URL -r requirements.txt \\
    && python -m pip install --prefer-binary --index-url $PYPI_URL -e .

FROM base as test
"""
    install_dev_package_str = (
        "RUN python -m pip install --prefer-binary --index-url $PYPI_URL -r requirements_dev.txt .[tests]"
    )
    assert file_copy_str in dockerfile_text
    assert install_package_str in dockerfile_text
    assert install_dev_package_str in dockerfile_text


@pytest.mark.parametrize(
    "build_backend",
    [
        ("setuptools", "\n@task(pre=[clean])\ndef release_twine(\n"),
        ("poetry", "\n@task(pre=[clean, dist])\ndef release_poetry(\n"),
    ],
)
def test_bake_build_backend_invoke_build_task(cookies: Cookies, build_backend: List[Tuple[str]]) -> None:
    """
    It tests that the build system is correctly set in the `pyproject.toml` file

    :param Cookies cookies: The fixture that returns a Cookiecutter instance
    :param build_backend: A tuple of the build backend option and the string that should be in the pyproject.toml file
    """
    build_option, invoke_build_task_str = build_backend
    result = bake_result(cookies, extra_context={
                         "build_backend": build_option})
    tasks_file_path = result.project_path / "tasks.py"
    tasks_file = tasks_file_path.read_text()
    assert invoke_build_task_str in tasks_file


@pytest.mark.parametrize(
    "build_backend",
    [
        (
            "setuptools",
            "\n  stage: publish_pypi\n  script:\n    - |\n      poetry run invoke release-twine \\\n",
        ),
        (
            "poetry",
            "\n  stage: publish_pypi\n  script:\n    - |\n      poetry run invoke release-poetry \\\n",
        ),
    ],
)
def test_bake_build_backend_gitlab_ci_pypi_publish_stage(cookies: Cookies, build_backend: List[Tuple[str]]) -> None:
    """
    It tests that the build system is correctly set in the `pyproject.toml` file

    :param Cookies cookies: The fixture that returns a Cookiecutter instance
    :param build_backend: A tuple of the build backend option and the string that should be in the pyproject.toml file
    """
    build_option, invoke_build_task_str = build_backend
    result = bake_result(cookies, extra_context={
                         "build_backend": build_option})
    gitlab_ci_file_path = result.project_path / ".gitlab-ci.yml"
    gitlab_ci_file = gitlab_ci_file_path.read_text()
    extends_str = "\n  extends:\n    - .run_on_release_tag\n"
    assert invoke_build_task_str in gitlab_ci_file
    assert extends_str in gitlab_ci_file
    if build_backend == "setuptools":
        tox_setting = "\nenvlist = py3.9, py3.10, docs\nskipsdist = true\n\n"
        assert tox_setting in gitlab_ci_file
