# {{ cookiecutter.project_name }}

{{ cookiecutter.project_short_description }}

## Features

## Installation

    pip install {{ cookiecutter.project_slug }} --index-url https://gitlab.com/api/v4/projects/<gitlab-project-id>/packages/pypi/simple

## Usage

    {{ cookiecutter.project_slug }} --help 

## Development

    git clone gitlab.com/{{ cookiecutter.project_gitlab_group }}/{{cookiecutter.project_name_kebab}}

    # create a virtual environment and activate it then run

    pip install -e .[dev]

    # run unittests

    invoke test   # use the invoke environment to manage development
    

## Documentation

The Documentation can be found here: [https://{{ cookiecutter.project_gitlab_group }}.gitlab.io/{{cookiecutter.project_name_kebab}}](https://{{ cookiecutter.project_gitlab_group }}.gitlab.io/{{ cookiecutter.project_name_kebab }}) or [{{ cookiecutter.project_name_kebab }}.gitlab.io]({{ cookiecutter.project_slug }}.gitlab.io/)


## Credits

This package was created with [Cookiecutter](https://github.com/audreyr/cookiecutter)
 and the [gitlab.com/opensourcelab/software-dev/cookiecutter-pypackage](https://gitlab.com/opensourcelab/software-dev/cookiecutter-pypackage) project template.



