#!/usr/bin/env python
"""Tests for `{{ cookiecutter.project_slug }}` package."""
# pylint: disable=redefined-outer-name
from {{cookiecutter.project_slug}}.{{cookiecutter.project_slug}}_interface import GreeterInterface
from {{cookiecutter.project_slug}}.{{cookiecutter.project_slug}}_impl import HelloWorld

def test_GreeterInterface():
    """ testing the formal interface (GreeterInterface)
    """
    assert issubclass(HelloWorld, GreeterInterface)

def test_HelloWorld():
    """ Testing HelloWorld class
    """
    hw = HelloWorld()
    name = 'yvain'
    assert hw.greet_the_world(name) == f"Hello world, {name} !"

