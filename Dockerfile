# Dockerfile to build base Docker image
# Example to build:
# docker build --build-arg PYTHON_BASE=3.9 --build-arg PYPI_URL_GENOMICS=https://artifactory.aws.gel.ac/artifactory/api/pypi/pypi/simple --platform linux/amd64 -f poetry.Dockerfile -t registry.gitlab.com/genomicsengland/opensource/templates/cookiecutter-pypackage/poetry:latest .
# Example to push:
# docker push registry.gitlab.com/genomicsengland/opensource/templates/cookiecutter-pypackage/poetry:latest
ARG PYTHON_BASE

FROM python:${PYTHON_BASE} as base
ARG PYPI_URL

WORKDIR /src

ENV PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1 \
    POETRY_NO_INTERACTION=1 \
    POETRY_VIRTUALENVS_CREATE=0 
#\    PIP_INDEX_URL=$PYPI_URL

COPY pyproject.toml .
COPY poetry.lock .

#RUN sed -i 's|http://|https://artifactory.aws.gel.ac/artifactory/apt_|g' /etc/apt/sources.list

RUN apt-get update -y \
    && pip install -Iv --prefer-binary --upgrade pip poetry \
    && poetry install --no-interaction --no-root
