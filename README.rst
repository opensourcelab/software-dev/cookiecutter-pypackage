===============================================
Cookiecutter PyPackage - OpenSourceLab Edition
===============================================

Cookiecutter_ template for a Python package, based on the `genomicsengland-cookiecutter-pypackage`_, 
which is based on the `briggySmalls/cookiecutter-pypackage`_ fork of `audreyr/cookiecutter-pypackage`_ .

* GitLab repo: https://gitlab.com/opensourcelab/software-dev/cookiecutter-pypackage
* Free software: BSD license

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _genomicsengland-cookiecutter-pypackage: https://gitlab.com/genomicsengland/opensource/templates/cookiecutter-pypackage
.. _briggySmalls/cookiecutter-pypackage: https://github.com/briggySmalls/cookiecutter-pypackage/
.. _audreyr/cookiecutter-pypackage: https://github.com/audreyr/cookiecutter-pypackage

Features
--------

This template has all of the features of the original `audreyr/cookiecutter-pypackage`_, plus the following:

* Dependency tracking using poetry_.
* Linting provided by both pylint_ (optional) and flake8_.
* Type checking provided by mypy_ (optional).
* Formatting provided by black_ and isort_.
* Security checks provided by bandit_ and safety_.
* Documentation generation provided by Sphinx_.
* Autodoc your code with `Google docstring style`_.
* Semantic versioning management provided by bump2version_.
* All development tasks (linting, format checking, security checking, testing, docs generation) wrapped up in a python CLI by invoke_ and optionally executed by Tox_.
* Generate Docker containers for development/CI automation using invoke or make.
* GitLab CI configuration autogenerated from the template (assumes presence of certain CI variables).
* docker-compose support

Quickstart
----------

Install the latest Cookiecutter if you haven't installed it yet (this requires
Cookiecutter 1.4.0 or higher)::

    pip install -U cookiecutter

Generate a Python package project::

    cookiecutter gl:opensourcelab/software-dev/cookiecutter-pypackage

Then:

* Create a repo and put it there.
* Install the dev requirements into a virtualenv. (``poetry install``)
* Release your package by pushing a new tag to master.
* Get your code on! 😎 Add your package dependencies as you go, locking them into your virtual environment with ``poetry add``.

.. _`pip docs for requirements files`: https://pip.pypa.io/en/stable/user_guide/#requirements-files
.. _Register: https://packaging.python.org/tutorials/packaging-projects/#uploading-the-distribution-archives

For more details, see the `cookiecutter-pypackage tutorial`_.

.. _`cookiecutter-pypackage tutorial`: https://genomicsengland.gitlab.io/opensource/templates/cookiecutter-pypackage/tutorial.html

.. _invoke: http://www.pyinvoke.org/
.. _isort: https://pypi.org/project/isort/
.. _black: https://github.com/psf/black/
.. _flake8: https://pypi.org/project/flake8/
.. _pylint: https://www.pylint.org/
.. _mypy: https://pypi.org/project/mypy/
.. _bandit: https://github.com/PyCQA/bandit
.. _safety: https://github.com/pyupio/safety
.. _poetry: https://python-poetry.org/
.. _Tox: https://tox.wiki/en/latest/index.html
.. _Sphinx: http://sphinx-doc.org/
.. _Google docstring style: https://www.sphinx-doc.org/en/master/usage/extensions/example_google.html
.. _pydocstyle: https://pypi.org/project/pydocstyle/
.. _bump2version: https://github.com/c4urself/bump2version/
.. _PyPi: https://pypi.python.org/pypi
